@extends('layouts.app')

@section('content')
<div class="container-frame">

@if (Auth::check())
        <div class="block">
            <video id="video" width="400" height="300"></video>
            <button id="startbutton"><span class="iconSpan"><img src='img/camera.png' id="cameraIcon" alt="camera"></span>Take a snap</button>
            <p class="hinttxt"><span class="notetxt">Note :</span> Take as much snaps as you want until it's the perfect one</p><br>
            <br>
        </div>

        <div class="block">
            <canvas class="canvas" id="canvas" width="400" height="300"></canvas><br>
            <img id="photo" disabled=true  style="display:none;"><br>
            <button id="savepicture"><span class="iconSpan"><img src='img/saving.png' id="cameraIcon" alt="camera"></span>Save picture</button>
            <p class="hinttxt" id="savehint"><span class="notetxt">Note :</span> You can't go back from this step. Careful !</p><br>
        </div>



        <div class="block filterblock">
            <form id="filterform" method="post" action="{{route('filterpicture')}}">
            {{ csrf_field() }}
                <div class="filterlist">
                    <?php $cpt = 0 ?>
                @forelse ($filters as $filt)

                    <label class="filterdisplay" style="background-image: url('{{$filt->url}}'); background-size: 100% 100%;">
                        <input type="checkbox"  class="filterchoice" name="selectedfilter" id="selectedfilter" value="{{$filt->url}}">
                    </label> 
                @empty
                    <p class="notfound">No filters available yet.</p><br><br>
                @endforelse
                </div>

                <input type="hidden" value="true" name="launchfilter">

                <button id="filterbutton" type="submit"><span class="iconSpan" name="submitfilter"><img src='img/mask.png' id="cameraIcon" alt="camera"></span>Add Filter</button>

                <p class="hinttxt" id="savehint"><span class="notetxt">Note :</span>Select which filter to apply on your picture.</p><br>
            </form>
        </div>

        <div class="loader-modal"></div>

@else 
    <script>window.location = "/login";</script>
@endif
</div>

    <script type="text/javascript">
        /* Loading - Please wait */
        $('#filterbutton').click(function() {
        $('#filterform').css('border','5px solid red'); // Ne fonctionne pas car le PHP est envoyé et bloque l'affichage AVANT que l'évènement ne soit exécuté
        $('.loader-modal').append("<img class='ajax-loader' src='{{asset('ajax-loader.gif')}}'>");  // Idem
                    });


    </script>

@endsection
