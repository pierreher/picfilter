@extends('layouts.app')

@section('content')

<div class="container-frame">
    <div id="allfriends">
        <h2>{{$user->name}}'s friends :</h2>
        @if(empty($friends))
            <h4 class="notfound" style="text-align:center;">No friends yet !</h4>
        @else
            <?php $cpt = 0; ?>

            @foreach($friends as $frd)
                @if($cpt < count($friends))
                    <p class="userdisplay">{{$frd[0]->name}}<span class="iconSpan">
                        <a class="friendmanage" href="/friends/delete/{{$frd[0]->id}}"><img src='img/delete.png' id="cameraIcon" alt="delete friend"> Delete friend</a></span></p>
                        <br>
                @endif
                <?php $cpt++ ?>
            @endforeach
        @endif
        <h2 class="topspace">Look for a friend :</h2>
            <form action="{{route('searchuser')}}" method="post">
                {{ csrf_field() }}
                <input type="text" name="searchuser" id="searchuser" placeholder="Search for user...">
                <input class="searchbutton" type="submit" value="Search">
            </form>
        <div class="result-user">
            @if($searchActivated==0)
                <p></p>
            @elseif($searchActivated==1)
                @forelse($results as $res)
                    @if($res->name == 'admin')
                        <h4 class="notfound" style="text-align:center;">No user found with that name.</h4>
                    @else
                        <p class="userdisplay">{{$res->name}}<span class="iconSpan">

                        @if($alreadyFriends)
                            Already friends
                        @else
                            <a class="friendmanage"  href="/friends/{{$res->id}}"><img src='img/add.png' id="cameraIcon" alt="add friend"> Add friend</a></span></p>
                            <br>
                        @endif
                    @endif
                @empty
                    <h4 class="notfound" style="text-align:center;">No user found with that name.</h4>
                @endforelse
            @endif
        </div>
    </div>
</div>


@endsection