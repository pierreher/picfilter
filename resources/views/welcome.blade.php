<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PicFilter</title>
           <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #0072C6;
                color: white;
                font-family: 'Raleway', sans-serif;
                height: 100%;
                margin: 0;
            }
            .title {
                font-family: Pacifico, sans-serif;
            }

            .full-height {
                height: 90vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 75px;
            }

            .flex-center .links > a {
                color: white;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
            h5 {
                font-weight: bold;
            }
            .numlist {
                font-family: Pacifico,Pavanam, sans-serif;
                font-size: 16px;
                border: 2px solid white;
                padding: 0.05em 0.7em;
                border-radius: 20px;
                margin-right: 1em;
                margin-top: 1em;
                background-color: white;
                color: #0072C6;
                line-height: 32px;
            }
            #prezlist li {
                margin-bottom: 0.4em;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    PicFilter
                </div>

                <div class="example">
                    <div class="left-example">
                        <h5>Appliquez facilement un filtre à votre image :</h5>
                        <ul id="prezlist">
                            <li><span class="numlist">1</span>Capturez votre image</li>
                            <li><span class="numlist">2</span>Sauvegardez-la</li>
                            <li><span class="numlist">3</span>Sélectionnez un filtre</li>
                            <li><span class="numlist">4</span>Visualisez le résultat !</li>
                        </ul>
                        <p>Un système de gestion de votre contenu vous permet également d'organiser, télécharger et partager vos photos.</p>
                        <div class="liens">
                            <a href="{{route('home')}}">Go !</a>
                            <a target="_blank" href="https://bitbucket.org/pierreher/picfilter/src">BitBucket</a>
                        </div>
                    </div>
                    <div class="right-example">
                        <img id="imgexample" src="img/example2.png" alt="example">
                    </div>

                </div>

            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
