@extends('layouts.app')

@section('content')

<div class="container-frame">
@if (Auth::check())
    <div id="allpics">
    <h2>{{$user->name}}'s pictures :</h2>
    @if($pictures->isEmpty())
            <h4 class="notfound">No picture found.</h4>
    @else
        @foreach ($pictures as $pic)
        <div class="picframe">
            <img src="{{ $pic->url }}" alt="picture{{$pic->id}}" class="userpic">
            <div class="picdetails">
                <p>Created on : {{date('F d, Y', strtotime($pic->created_at))}}</p>
                <a href=""><img class="picicon" src="img/share.png" alt="share" title="Share"></a>
                <a id="deletepic" href="{{route('destroypic',$pic->id)}}"><img class="picicon" src="img/delete.png" alt="delete" title="Delete"></a>
            </div>
        </div>
        @endforeach
    @endif
    </div>

@else
    <script>window.location = "/login";</script>
@endif

</div>
@endsection