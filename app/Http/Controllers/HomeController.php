<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filter;
use Storage;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allfilters = Filter::all();
        return view('home',["filters"=>$allfilters]);
    }
}
