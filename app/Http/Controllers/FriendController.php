<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\UserFriend;
use DB;

class FriendController extends Controller
{
    private function friendlist($user) {
        $friends = UserFriend::select('friend_id')->where('user_id',$user->id)->get();
        $userfriends = [];
        foreach($friends as $frd) {
            $current_friend = User::find($frd);
            array_push($userfriends,$current_friend);
        }
        return $userfriends;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    if (Auth::check()) {
        $user = User::find(Auth::id());
        $userfriends = $this->friendlist($user);
        $searchActivated = 0;

        return view('user_friends',["user"=>$user,"friends"=>$userfriends,"searchActivated"=>$searchActivated]); 
    } else {
        return view('home');
    }
    }

    /**
     * Search user
     * 
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $user = User::find(Auth::id());
        $userfriends = $this->friendlist($user);
        $results = User::select('id','name')->where('name',$request->searchuser)->get();
        $searchActivated = 1;

            $checkifexists = UserFriend::where(['user_id'=>$user->id,'friend_id'=>$results[0]->id])->exists();

        return view('user_friends',['results'=>$results,"user"=>$user,"friends"=>$userfriends,'searchActivated'=>$searchActivated,'alreadyFriends'=>$checkifexists]);
    }

    public function addfriend($id) {
        if (Auth::check()) {
            $user = User::find(Auth::id());
            $userfriends = $this->friendlist($user);
            $searchActivated = 0;

            UserFriend::create(['user_id'=>Auth::id(),'friend_id'=>$id]);

            return redirect()->route('myfriends',["user"=>$user,"friends"=>$userfriends,"searchActivated"=>$searchActivated]);
        } else {
            return view('home');
        }
    }
    public function deletefriend($id) {
        if (Auth::check()) {
            $user = User::find(Auth::id());
            $userfriends = $this->friendlist($user);
            $searchActivated = 0;
    
            $friend = UserFriend::where('friend_id',$id);
            $friend->delete();

            return redirect()->route('myfriends',["user"=>$user,"friends"=>$userfriends,"searchActivated"=>$searchActivated]);
        } else {
            return view('home');
        }
    }

}
