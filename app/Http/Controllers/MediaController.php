<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use File;
use App\Lib\Pixlab;
use Auth;
use App\User;
use App\Picture;
use App\Filter;

class MediaController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        $pictures = $user->picture;
        return view('user_pictures',["pictures"=>$pictures,"user"=>$user]);
    }

    public function savePicture()
    {
        //flush all temp files
        $files = glob('img/uploads/temp/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }   
        session_unset();

        if ( is_null($_POST['launchfilter']) ) {
            echo('Error : authentication failed.'); die;
        }
        else {
            echo('launchfilter not null - ');
            if ( $_POST['launchfilter']=='false' ) {
                echo('launchfilter false');
                session_start();
                $post_canvas = $_POST['imgBase64'];
                //$post_canvas = str_replace('data:image/png;base64,','',$post_canvas);
                $post_canvas = str_replace(' ','+',$post_canvas);
                $fileData = base64_decode($post_canvas);
            
                //Creating the 3 upload dirs
                $savedir = 'img/uploads/temp/';
                if(!File::exists($savedir)) {
                    if (!File::makeDirectory($savedir,0777,true)) {
                        echo('Error : couldn\'t create temp directory');
                        die; 
                    }
                }

                $logdir = 'img/uploads/logs/';
                if(!File::exists($logdir)) {
                    if (!File::makeDirectory($logdir,0777,true)) {
                        echo('Error : couldn\'t create logs directory');
                        die; 
                    }
                }

                $resultdir = 'img/uploads/results/';
                if(!File::exists($resultdir)) {
                    if (!File::makeDirectory($resultdir,0777,true)) {
                        echo('Error : couldn\'t create results directory');
                        die; 
                    }
                }

                $savename = md5(uniqid(rand(), true)).'.png';
                $fileName = $savedir.$savename;

                file_put_contents($fileName,$fileData);

                $_SESSION['file_temp'] = $fileName;   
                
            }
            else {
                echo('Error: picture to filter missing.');
            }
        }
    }
    /*
    * Uploads all filters in DB to PixLab and replace its url in DB.
    */
    public function filterupload() {
        $pix = new Pixlab('3d3d73db469f4bad0b06674ec63473e8');

        $allfilters = Filter::all();
        $cpt = 1;
        
        foreach($allfilters as $filt) {
            if(!$pix->post('store', array('comment' => 'Filter to upload'),$filt->url)) {
                echo $pix->get_error_message()."<br>";
                die;
            } else {
                file_put_contents('img/uploads/logs/upload_log.txt',$pix->json->link.',',FILE_APPEND);
                $newfilt = Filter::find($cpt);
                var_dump($newfilt->url);
                $newfilt->url = $pix->json->link;
                $newfilt->save();
                
            }            
            echo '<pre>'.Filter::all().'</pre>';
            # link to the uploaded img
            $img = $pix->json->link;
            $cpt++;
        }
        die();
    }

    public function filterPicture() {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        session_start();
        $pix = new Pixlab('3d3d73db469f4bad0b06674ec63473e8');
        $local_img = $_SESSION['file_temp'];
        $pix->switch_to_http();  

        /*Storing the img */
        if(!$pix->post('store', array('comment' => 'Image to upload'),$local_img)) {
            echo $pix->get_error_message()."<br>";
            die;
        } else {
            file_put_contents('img/uploads/logs/upload_log.txt',$pix->json->link.',',FILE_APPEND);
        }
        # link to the uploaded img
        $img = $pix->json->link;

            $filter = $_POST['selectedfilter'];
            $fid = Filter::where('url',$filter)->get();
            foreach($fid as $f){
                $filterid = $f->id;
            }
            //$filter='/img/filters/mask.png';

            switch($filterid) {
                case 1 : $yadjust = 125; // cat
                         $xadjust = 38;
                         $widthadjust = 40;
                    break;
                case 2 : $yadjust = 130; // deer
                         $xadjust = 90;
                         $widthadjust = 100;
                    break;
                case 3 : $yadjust = 105; // dog & nose
                         $xadjust = 50;
                         $widthadjust = 30;
                    break;
                case 4 : $yadjust = 130; // dog
                         $xadjust = 50;
                         $widthadjust = 50;
                    break;
                case 5 : $yadjust = 155; // unicorn
                         $xadjust = 55;
                         $widthadjust = 70;
                    break;
                case 6 : $yadjust = 110; // flowers
                         $xadjust = 30;
                         $widthadjust = 70;
                    break;
                case 7 : $yadjust = 28; // mask
                         $xadjust = 32;
                         $widthadjust = 20;
                    break;
                case 8 : $yadjust = -41; // rainbow
                         $xadjust = 27;
                         $widthadjust = 40;
                    break;
                case 9 : $yadjust = 140; // rabbit
                         $xadjust = 60;
                         $widthadjust = 70;
                    break;
                case 10 : $yadjust = 140; // rabbit 2
                          $xadjust = 65;
                          $widthadjust = 90;
                    break;
            }

        /* Grab the face landmarks  */
        if( !$pix->get('facelandmarks',[
            'img' => $img
            ]) ){
            echo $pix->get_error_message()."<br>";
            die;
        }

        $total = count($pix->json->faces); # Total detected faces
        //print("<br><br>".$total." faces were detected<br>");
        $snaps = [];

        # Iterate all over the detected faces
        foreach ($pix->json->faces as $face){
            array_push($snaps,$face);
            $lone_snap = $face;
        }
            $cords = [];
            echo('countingfaces - ');
            # Make a quick Snapchat filter on top of the last detected face
            if ($total < 1){
                echo ("No faces were detected");
                die;
            }
            elseif ($total > 1) {
                foreach($snaps as $snap) {
                    # Resize the filter which is quite big right now to exactly the face width using smart resize.
                    if( !$pix->get('smartresize',[
                        'img' => $filter,
                        'width' => $snap->rectangle->width + 20, # Face width
                        'height' => 0 # Let Pixlab decide the best height for this picture
                        ]) ){
                        echo $pix->get_error_message()."<br>";
                    }else{
                        $filter = $pix->json->link;
                    }

                    array_push($cords,array(
                        'img' => $filter,
                        'x' => $snap->landmarks->eye->left_outer->x-$xadjust,
                        'y' => $snap->landmarks->eye->right_outer->y-$yadjust
                        /* Adjust for optimal effect */
                    ));
                }
            }
            else {
                    # Resize the filter which is quite big right now to exactly the face width using smart resize.
                    if( !$pix->get('smartresize',[
                        'img' => $filter,
                        'width' => $lone_snap->rectangle->width + $widthadjust, # Face width
                        'height' => 0 # Let Pixlab decide the best height for this picture
                        ]) ){
                        echo $pix->get_error_message()."<br>";
                    }else{
                        $filter = $pix->json->link;
                    }

                    array_push($cords,array(
                        'img' => $filter,
                        'x' => $lone_snap->landmarks->eye->left_outer->x-$xadjust,
                        'y' => $lone_snap->landmarks->eye->right_outer->y-$yadjust /* Adjust for optimal effect */
                    ));
            }
            echo('resized - ');

            # Finally, Perform the composite operation
            if( !$pix->post('merge',[
                'src' => $img,
                'cord' => $cords
                ]) ){
                echo $pix->get_error_message();
            }else{
                # Optionally call blur, oilpaint, grayscale for more stuff..
                $result = $pix->json->link;
                echo('end ');
                //updating the log
                file_put_contents('img/uploads/logs/upload_log.txt',$result.',',FILE_APPEND);

                //downloading the result + inserting local url to DB
                $pixlabName = 'img/uploads/results/'.substr($result,24,21);
                file_put_contents($pixlabName,file_get_contents($result));

                $picture = new Picture;
                $picture->url = $pixlabName;
                $picture->user_id = Auth::user()->id;
                $picture->save();
            }

            // replace view with :
            return redirect()->route('mypictures');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find(Auth::id());
        $pic = Picture::find($id);
        if(is_file($pic->url)) {
            unlink($pic->url);
        }
        $pic->delete();
        $allpics = Picture::all();
        return redirect()->route('mypictures',["pictures"=>$allpics,"user"=>$user]);
    }
}
