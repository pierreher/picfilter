<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFriend extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'friend_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function users() {
        return $this->belongsToMany('App\User');
    }
} 
