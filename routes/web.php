<?php

/*--------------------------------------------------------------------------
 Web Routes
--------------------------------------------------------------------------*/

/*========== PORTAL =============*/
Route::get('/', function () {
    return view('welcome');
});

/*========== AUTH =============*/
Auth::routes();

/*========== HOME =============*/
Route::get('/home', 'HomeController@index')->name('home');
    /*---- Displays the filter ----*/
    Route::get('/home/savepicture','HomeController@index');
    /*---- Save a pic from webcam ----*/
    Route::post('/home/savepicture', 'MediaController@savePicture')->name('savepicture');
    /*---- Upload new filters to PixLab ----*/
    Route::get('/home/filterupload/{url}/{id}','MediaController@filterupload')->name('filterupload');
    /*---- Merges filter and picture  ----*/
    Route::post('/home/filterpicture','MediaController@filterPicture')->name('filterpicture');
    /*---- Guards the GET method ----*/
    Route::get('/home/filterpicture','HomeController@index');


/*========== PERSONNAL PICTURES ============*/
Route::get('/mypictures','MediaController@index')->name('mypictures');
    /*---- Delete a picture ----*/
    Route::get('/mypictures/delete/{id}','MediaController@destroy')->name('destroypic');

/*========== FRIENDS PAGE =============*/
Route::get('/friends','FriendController@index')->name('myfriends');
    /*---- Search a friend ----*/
    Route::post('/friends','FriendController@search')->name('searchuser');
    Route::get('/friends/{id}','FriendController@addfriend')->name('addfriend');
    Route::get('/friends/delete/{id}','FriendController@deletefriend')->name('deletefriend');